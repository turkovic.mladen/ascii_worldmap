#!/bin/bash
# plot the location on ACII World Map

help='
World Map [latitude] [longitude] < -v, --verbatim >
World Map -h

 Draw a location on the map of the world (+/- few 1000 kilometers :)
 based on latitude and longitude location expressed numerically.
 
 Since the map spans from 85°N to 65°S all locations closer to the 
 poles will be truncated to the map.
 
[latitude]  numeric latitude value for location (-65 to 85)
            for North use positive values (e.g. 12.34°N enter 12.34),
            for South use negative (e.g. 45.67°S enter -45.67)

[longitude] numeric longitude value for location (-180 to 180)
            for East use positive values (e.g. for 35.51°E enter 35.51),
            for West use negative (e.g. for 25.78°W enter -25.78)

-v, --verbatim print input data and date

-h, --help  print this help
'

if [[ $1 = -h || $1 = --help ]]; then
   echo "$help"
   exit
fi

latitude=$1
longitude=$2

if [[ ${latitude%.*} -gt 90 || ${latitude%.*} -lt -90 ]]; then
   echo "Latitude value $latitude is not valid!"
   exit
fi

if [[ ${longitude%.*} -gt 180 || ${longitude%.*} -lt -180 ]]; then
   echo "Longitude value $longitude is not valid!"
   exit
fi


if [[ $3 = -v || $3 = --verbatim ]]; then

	echo "Date: " $(date)

	# remove negative sign if South
	if [ ${latitude%.*} -lt 0 ]; then
		echo " > Latitude : ${latitude:1} °S"
	else
		echo " > Latitude : $latitude °N"
	fi

	# remove negative sign if West
	if [ ${longitude%.*} -lt 0 ]; then
		echo " > Longitude: ${longitude:1} °W"
	else
		echo " > Longitude: $longitude °E"
	fi

fi

# ########################################### #
# from: http://www.asciiworld.com/-Maps-.html #
# ########################################### #

# inverted color & bold (if possible)
pos_on="\e[7m\e[1m"
pos_off="\e[0m"

world_map="
+---------------------------------------------------------------------+
|.              ,_   .  ._. _.  .                                     |
|           , _-\','|~\~      ~/      ;-'_   _-'     ,;_;_,    ~~-    |
|  /~~-\_/-'~'--' \~~| ',    ,'      /  / ~|-_\_/~/~      ~~--~~~~'--_|
|  /              ,/'-/~ '\ ,' _  , '|,'|~                   ._/-, /~ |
|  ~/-'~\_,       '-,| '|. '   ~  ,\ /'~                /    /_  /~   |
|.-~      '|        '',\~|\       _\~     ,_  ,               /|      |
|          '\        /'~          |_/~\\\\\,-,~  \ \"         ,_,/ |      |
|           |       /            ._-~'\_ _~|              \ ) /       |
|            \   __-\           '/      ~ |\  \_         /  ~         |
|  .,         '\ |,  ~-_      - |          \\\\\_' ~|  /\  \~ ,          |
|               ~-_'  _;       '\           '-,   \,' /\/  |          |
|                 '\_,~'\_       \_ _,       /'    '  |, /|'          |
|                   /     \_       ~ |      /          \  ~'; -,_.    |
|                   |       ~\        |    |  ,         '-_, ,; ~ ~\  |
|                    \,      /        \    / /|            ,-, ,   -, |
|                     |    ,/          |  |' |/          ,-   ~ \   '.|
|                    ,|   ,/           \ ,/              \       |    |
|                    /    |             ~                 -~~-, /   _ |
|                    |  ,-'                                    ~    / |
|                    / ,'                                      ~      |
|                    ',|  ~                                           |
|                      ~'                                             |
+---------------------------------------------------------------------+
"
# map goes to approx. 85°N and 65°S; 

# truncating valid South and North latitudes
if [[ ${latitude%.*} -gt 85 ]]; then
	latitude=85
fi

if [[ ${latitude%.*} -lt -65 ]]; then
	latitude=-65
fi

# the equator is located on line 13 (85 / 13 = 6.538)
line_on_map=$(( ( ( (90 - ${latitude%.*})*100 / 654 ) ) ))

# map covers 360° in 70 columns i.e. (360 / 70 = 5.143)
column_on_map=$(( ( ( ( ${longitude%.*} + 180 ) * 100 / 514 ) ) ))

pos_on_map=$(( ($line_on_map * 72) + $column_on_map))

# fixing extra length due to escape chars :(
if [ $pos_on_map -gt 545 ]; then
	pos_on_map=$(($pos_on_map + 1))
fi
if [ $pos_on_map -gt 766 ]; then
	pos_on_map=$(($pos_on_map + 1))
fi

# #################################### #
# output the position on the World Map #
# #################################### #


## get curent location char; to skip EOL '\n' char
charLoc=${world_map:$pos_on_map:1}

## get previous char; to escape '\' char
charLocPrev=${world_map:$pos_on_map-1:1}

# echo -e $("$world_map" | sed s/$charLoc/+/$pos_on_map)

if [ "${charLoc-}" = $'\n' ]; then
	echo -e "${world_map:0:$pos_on_map+1}"$pos_on"+"$pos_off"${world_map:$pos_on_map+2}"
elif [ "${charLocPrev-}" = $'\\' ]; then
	echo -e "${world_map:0:$pos_on_map-1}""\ "$pos_on"+"$pos_off"${world_map:$pos_on_map+2}"
	#echo -e "${world_map:0:$pos_on_map-1}""\\\\"$pos_on"+"$pos_off"${world_map:$pos_on_map+2}"
else
	echo -e "${world_map:0:$pos_on_map}"$pos_on"+"$pos_off"${world_map:$pos_on_map+1}"
fi
